{-Gruppe 01: Ajein Loganathan, Lennart Ferlings, Sebastian Hinrichs
Blatt 02, Abgabe: 11.11.2019
-}
-- Die folgende Zeile können Sie ignorieren (aber nicht löschen)
module P043 where

-- ------------------Aufgabenteil a-----------------

--Ein Datentyp Punkt bestehend aus einem x-Wert und einem y-Wert
data Point = MkPoint{
                    pX :: Float,
                    pY :: Float}
                    deriving (Show)

--Ein Datentyp Vektor bestehend aus einem x-Wert und einem y-Wert
data Vector = MkVector{
                    vX :: Float,
                    vY :: Float}
                    deriving (Show)

-- ------------------Aufgabenteil b-----------------

--Die Funktion addiert auf einen Punkt einen Vektor
translate :: Point -> Vector -> Point
translate (MkPoint p1 p2) (MkVector v1 v2) =  MkPoint (p1 + v1) (p2 + v2)

--Die Funktion skaliert den übergebenen Punkt mit einem Faktor
scale :: Point -> Float -> Point
scale (MkPoint p1 p2) factor = MkPoint (p1 * factor) (p2 * factor)

-- ------------------Aufgabenteil c-----------------

--Definition der Klasse Polygon und Spezifikation ihrer Operationen
class Polygon a where

    area :: a -> Float

    translate_poly :: a -> Vector -> a

    scale_poly :: a -> Float -> a

--Definiert den Datentyp Triangle bestehend aus drei Punkten
data Triangle = MkTriangle {
                        tP1 :: Point,
                        tP2 :: Point,
                        tP3 :: Point}
                        deriving(Show)

--Definiert den Datentyp Quad bestehend aus vier Punkten
data Quad = MkQuad{
                    qP1 :: Point,
                    qP2 :: Point,
                    qP3 :: Point,
                    qP4 :: Point}
                    deriving(Show)


-- ------------------Aufgabenteil d-----------------

instance Polygon Triangle where

    -- Flächenberechnung unter Anwendung des Satz des Heron
    area triangle = sqrt(getS triangle * (getS triangle - getDistance (tP1 triangle) (tP2 triangle)) * (getS triangle - getDistance (tP2 triangle) (tP3 triangle)) * (getS triangle - getDistance (tP3 triangle) (tP1 triangle)))
    
    --Verschiebt das Dreieck um einen Vektor
    translate_poly triangle vector = MkTriangle (translate (tP1 triangle) vector) (translate (tP2 triangle) vector) (translate (tP3 triangle) vector)
    
    --Multipliziert jeden Punkt eines Dreiecks mit einem Skalar
    scale_poly triangle scalar = MkTriangle (scale (tP1 triangle) scalar) (scale (tP2 triangle) scalar) (scale (tP3 triangle) scalar)

getDistance :: Point -> Point -> Float
getDistance p1 p2 = sqrt( ((pX p1) - (pX p2))^2 + ((pY p1) - (pY p2))^2 )

--Berechnet das s für den Satz des Heron. Dabei entspricht s dem halben Umfang des Dreiecks
getS :: Triangle -> Float
getS triangle = (getDistance (tP1 triangle) (tP2 triangle) + getDistance (tP2 triangle) (tP3 triangle) + getDistance (tP3 triangle) (tP1 triangle)) / 2

instance Polygon Quad where

    --Berechnet die Fläche des Vierecks mittels der Gaußschen Trapezformel
    area quad = 0.5 * abs( ( (pY (qP1 quad)) - (pY (qP3 quad)) ) * ( (pX (qP4 quad)) - (pX (qP2 quad)) ) + ( (pY (qP2 quad)) - (pY (qP4 quad)) ) * ( (pX (qP1 quad)) - (pX (qP3 quad)) ) ) 
    
    --Verschiebt das Viereck um einen Vektor
    translate_poly quad vector = MkQuad (translate (qP1 quad) vector) (translate (qP2 quad) vector) (translate (qP3 quad) vector) (translate (qP4 quad) vector)
    
    --Multipliziert jeden Punkt eines Vierecks mit einem Skalar
    scale_poly quad scalar = MkQuad (scale (qP1 quad) scalar) (scale (qP2 quad) scalar) (scale (qP3 quad) scalar) (scale (qP4 quad) scalar)