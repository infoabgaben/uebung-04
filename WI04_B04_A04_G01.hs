{-Gruppe 01: Ajein Loganathan, Lennart Ferlings, Sebastian Hinrichs
Blatt 02, Abgabe: 11.11.2019
-}
-- Die folgende Zeile können Sie ignorieren (aber nicht löschen)
module P044 where

-- ------------------Aufgabenteil a-----------------

--Ein Typ, der die verschiedenen Werte der Karten definiert
data Rank = Seven | Eight | Nine | Ten | Jack | Queen | King | Ace
    deriving(Enum, Eq, Ord, Show)

--Ein Typ, der die verschiedenen Farben/Sorten der Karten definiert
data Suit = Diamond | Heart | Spade | Club
    deriving(Enum, Eq, Ord, Show)

--Eine Karte mit einem zugehörigem Wert und einer Farbe
data Card = MkCard {
            rank :: Rank,
            suit :: Suit}
            deriving(Eq, Show)

-- ------------------Aufgabenteil b-----------------

--Implementiert die compare Funktion aus Ord, sodass alle Vergleiche durchgeführt werden können
instance Ord Card where 
    compare c1 c2 = compare (rank c1, suit c1) (rank c2, suit c2)

-- ------------------Aufgabenteil c-----------------

--Ein Typ, bestehend aus drei Karten
data Hand = MkHand {
                card1 :: Card,
                card2 :: Card,
                card3 :: Card}
                deriving(Show, Eq)

--Prüft ob die drei Karten einer Hand denselben Wert haben
isTriple :: Hand -> Bool
isTriple hand = rank (card1 hand) == rank (card2 hand) && rank (card1 hand) == rank (card3 hand)

--Prüft ob mindestens zwei Karten einer Hand denselben Wert haben
isPair :: Hand -> Bool
isPair hand = rank (card1 hand) == rank (card2 hand) || rank (card1 hand) == rank (card3 hand) || rank (card2 hand) == rank (card3 hand)

--Prüft ob alle Karten einer Hand derselben Farbe angehören
isFlush :: Hand -> Bool
isFlush hand = suit (card1 hand) == suit (card2 hand) && suit (card1 hand) == suit (card3 hand)

--Bestimmt durch Überprüfung der einzelnen Möglichkeiten den Wert einer Hand
value :: Hand -> Integer
value hand 
            | isTriple hand = 2
            | isPair hand = 1
            | isFlush hand = 3
            | otherwise = 0
            
-- ------------------Aufgabenteil d-----------------

--Implementiert die compare Funktion aus Ord, sodass alle Vergleiche durchgeführt werden können.
--Dies geschieht durch den Vergleich des Values einer Hand (siehe c) oder falls dieses gleich ist
--durch den Vergleich der höchsten Kartenwerte einer Hand
instance Ord Hand where
    compare h1 h2 = if value h1 /= value h2 then 
                        compare (value h1) (value h2) 
                    else
                        compare (getHighestCard h1) (getHighestCard h2)

--Funktion, welche die höchste Karte einer Hand bestimmt
getHighestCard :: Hand -> Card
getHighestCard hand = max (max (card1 hand) (card2 hand)) (max (card1 hand) (card3 hand))