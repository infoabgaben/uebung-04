{-Gruppe 01: Ajein Loganathan, Lennart Ferlings, Sebastian Hinrichs
Blatt 02, Abgabe: 11.11.2019
-}
-- Die folgende Zeile können Sie ignorieren (aber nicht löschen)
module P045 where

-- ------------------Aufgabenteil a-----------------

--Definition des Datentyps Currency der entweder Euro, Dollar oder Yen darstellt
data Currency = Euro Integer Integer | Dollar Integer Integer | Yen Integer

-- ------------------Aufgabenteil b-----------------

--Implementierung der Anzeige von Currency
instance Show Currency where
    show (Euro e c) = show e ++ "," ++ (if( c < 10 ) then "0" else "") ++ show c ++ "E" -- Anmerkung: Eurozeichen führt zu Runtime Exception
    show (Dollar d c) = show d ++ "," ++ (if( c < 10 ) then "0" else "") ++ show c ++ "$"
    show (Yen y) = show y ++ "¥"

-- ------------------Aufgabenteil c-----------------

--Für jede Währung einzeln definierte Funktionen die normalisierte Werte zurückgeben
normalize :: Currency -> Currency

normalize (Euro e c) = Euro (e + floor(fromIntegral(c) / 100)) (c - floor(fromIntegral(c) / 100) * 100)
normalize (Dollar d c) = Dollar (d + floor(fromIntegral(c) / 100)) (c - floor(fromIntegral(c) / 100) * 100)
normalize (Yen y) = Yen y

-- ------------------Aufgabenteil d-----------------

--Die Funktionen rechnet jede Währung in Euro rum und gibt den Centbetrag in Euro zurück
toEuroCent :: Currency -> Integer
toEuroCent (Euro e c) = e * 100 + c
toEuroCent (Dollar d c) = round(fromIntegral(d * 100 + c) * 0.9)
toEuroCent (Yen y) = round(fromIntegral(y) * 0.0083)

--Die Funktionen rechnet jede Währung in Euro rum
toEuro :: Currency -> Currency
toEuro (Euro e c) = normalize (Euro 0 (toEuroCent (Euro e c)))
toEuro (Dollar d c) = normalize (Euro 0 (toEuroCent(Dollar d c)))
toEuro (Yen y) = normalize (Euro 0 (toEuroCent(Yen y)))

--Implementiert einen Vergleich von verschiedenen Währungen mittels Eq
--Beispiel: Es gilt 3€ ungleich 4€. Dies wird richtig bestimmt, auch bei verschiedenen Währungseinheiten. 
instance Eq Currency where
    (==) c1 c2 = toEuroCent c1 == toEuroCent c2

--Implementiert das Ordnen von Währungsbeträgen
--Beispiel: Es gilt 12$ sind weniger als 12€. Dies wird richtig bestimmt, auch bei verschiedenen Währungseinheiten. 
instance Ord Currency where
    compare c1 c2 = compare (toEuroCent c1) (toEuroCent c2)

